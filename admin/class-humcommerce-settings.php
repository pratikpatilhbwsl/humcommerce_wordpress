<?php
/**
 * The admin-specific settings of the plugin.
 *
 * @since      1.0.0
 *
 * @package    humcommerce
 * @subpackage humcommerce/admin
 */

/**
 * The admin-specific settings of the plugin.
 *
 * Defines plugin settings, fetch options saved for the plugin, validate input.
 *
 * @since      1.0.0
 *
 * @package    humcommerce
 * @subpackage humcommerce/admin
 */
class Humcommerce_Settings {

	/**
	 * The options for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $options    The current options for this plugin.
	 */
	private $options;

	/**
	 * Initialize the class.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page.
	 *
	 * @since 1.0.0
	 */
	public function add_plugin_page() {
		add_menu_page(
			'HumCommerce Settings',
			'HumCommerce',
			'administrator',
			'humcommerce-settings',
			array( $this, 'create_admin_page' ),
			esc_url( plugins_url( 'images/icon.png', __FILE__ ) )
		);

	}

	/**
	 * Options page callback.
	 *
	 * @since 1.0.0
	 */
	public function create_admin_page() {
		// Set class property.
		$this->options = get_option( 'humcommerce_options' );
		?>
		<div id="humcommerce-plugin-container">
			<div class="humcommerce-masthead">
				<div class="humcommerce-masthead__inside-container">
					<div class="humcommerce-masthead__logo-container">
						<img class="humcommerce-masthead__logo" src="<?php echo esc_url( plugins_url( '/images/logo.png', __FILE__ ) ); ?>" alt="humcommerce">
					</div>
				</div>
			</div>
			<div class="humcommerce-lower">

				<div class="humcommerce-box">
					<h2>Record visitors on your site</h2>
					<p>Select one of the options below to get started.</p>
				</div>
				<div class="humcommerce-boxes">
					<div class="humcommerce-box">
						<h3>Activate HumCommerce</h3>
						<div class="humcommerce-right">
							<form name="humcommerce_activate" action="https://www.humcommerce.com/" method="POST" target="_blank">
								<input type="hidden" name="redirect" value="plugin-signup">
								<input type="submit" class="humcommerce-button humcommerce-is-primary" value="Get your site ID">
							</form>
						</div>
						<p>Log in or sign up now.</p>
					</div>
					<div class="humcommerce-box">
						<div class="icon32" id="icon-options-general"><br></div>
						<form action="options.php" method="post">
							<?php
							if ( function_exists( 'wp_nonce_field' ) ) {
								wp_nonce_field( 'humcommerce-action_yep' );
							}
							?>
							<?php settings_fields( 'humcommerce_options' ); ?>
							<?php do_settings_sections( __FILE__ ); ?>
							<p class="submit">
								<input name="Submit" type="submit" class="humcommerce-button" value="<?php esc_attr_e( 'Save Changes' ); ?>"/>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Register and add settings.
	 *
	 * @since 1.0.0
	 */
	public function page_init() {
		register_setting(
			'humcommerce_options',
			'humcommerce_options',
			array( $this, 'humcommerce_options_validate' )
		);

		add_settings_section(
			'main_section',
			'',
			array( $this, 'section_text_fn' ),
			__FILE__
		);

		add_settings_field(
			'si',
			'Your site ID:',
			array( $this, 'setting_si_fn' ),
			__FILE__,
			'main_section'
		);

		add_settings_field(
			'host',
			'',
			array( $this, 'setting_host_fn' ),
			__FILE__,
			'main_section'
		);
	}

	/**
	 * Print the Section text.
	 *
	 * @since 1.0.0
	 */
	public function section_text_fn() {
		echo '
            <h3>Or enter your site ID</h3>
            <p>Already have your site ID? Enter it here. <a href="https://www.humcommerce.com/docs/find-site-id-humcommmerce-tool/" target="_blank">(What is site ID?)</a></p>
         ';
	}

	/**
	 * Get site ID from option array and print its value.
	 *
	 * @since 1.0.0
	 */
	public function setting_si_fn() {
		echo "<input required id='si' class='regular-text code' style='flex-grow: 1; margin-right: 1rem;' name='humcommerce_options[si]' size='40' type='number' value='" . esc_attr( $this->options['si'] ) . "' />";
	}

	/**
	 * Get HumCommerce Host from options array and prints its value.
	 *
	 * @since 1.0.0
	 */
	public function setting_host_fn() {
		echo "<input hidden id='host' class='regular-text code' style='flex-grow: 1; margin-right: 1rem;' name='humcommerce_options[host]' size='40' value='app.humdash.com' />";
	}

	/**
	 * Validate input with options.
	 *
	 * @since 1.0.0
	 *
	 * @param string $input Input to validate.
	 *
	 * @return mixed
	 */
	public function humcommerce_options_validate( $input ) {
		if ( empty( $input['si'] ) ) {
			add_settings_error( 'humcommerce_options', 'error_code', 'Please enter Site ID.' );
		} else {
			$input['si'] = wp_filter_nohtml_kses( $input['si'] );
			if ( ! is_numeric( $input['si'] ) ) {
				sanitize_text_field( $input['si'] );
				add_settings_error( 'humcommerce_options', 'error_code', 'Please enter valid Site ID.' );
				$input['si'] = '';
			}
		}
		return $input;
	}
}

if ( is_admin() ) {
	$my_settings_page = new Humcommerce_Settings();
}

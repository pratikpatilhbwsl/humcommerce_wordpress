<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    humcommerce
 * @subpackage humcommerce/admin
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    humcommerce
 * @subpackage humcommerce/admin
 */
class Humcommerce_Admin {

	/**
	 * The name of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $humcommerce    The name of this plugin.
	 */
	private $humcommerce;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $humcommerce       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $humcommerce, $version ) {

		$this->humcommerce = $humcommerce;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->humcommerce, plugin_dir_url( __FILE__ ) . 'css/humcommerce-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->humcommerce, plugin_dir_url( __FILE__ ) . 'js/humcommerce-admin.js', array( 'jquery' ), $this->version, false );

	}


}

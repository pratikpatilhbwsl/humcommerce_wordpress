<?php
/**
 * The public facing functionality of the plugin.
 *
 * @since 1.0.0
 *
 * @package humcommerce
 * @subpackage humcommerce/public
 */

/**
 * The public facing functionality of the plugin.
 *
 * @since 1.0.0
 *
 * @package humcommerce
 * @subpackage humcommerce/public
 */
class Humcommerce_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $humcommerce_custom    The ID of this plugin.
	 */
	private $humcommerce;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $humcommerce       The name of the plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $humcommerce, $version ) {

		$this->humcommerce_custom = $humcommerce;
		$this->version            = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Humcommerce_Custom_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Humcommerce_Custom_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->humcommerce_custom, plugin_dir_url( __FILE__ ) . 'css/humcommerce-custom-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Humcommerce_Custom_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Humcommerce_Custom_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->humcommerce_custom, plugin_dir_url( __FILE__ ) . 'js/humcommerce-custom-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * HumCommerce init functionality
	 *
	 * @since    1.0.0
	 */
	public function init_hum_commerce() {
		include_once ABSPATH . 'wp-admin/includes/plugin.php';
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			if ( is_product() ) {
				$product = get_page_by_path( get_post()->post_name, OBJECT, 'product' );
				if ( ! empty( $product ) ) {
					$product        = wc_get_product( $product );
					$category_terms = get_the_terms( $product->get_id(), 'product_cat' );
					$category_array = $this->get_categories_array( $category_terms );
				}
				$p_code = '
        _ha.push(["setEcommerceView",
                        "' . $product->get_sku() . '", 
                        "' . $product->get_title() . '", 
                        [' . $category_array . '], 
                        ' . get_post_meta( $product->get_id(), '_price', true ) . ' 
                        ]);';
				update_option( 'humcommerce_product_view', $p_code );
			}

			if ( is_product_category() ) {
				$c_code = '
        _ha.push(["setEcommerceView",
        productSku = false,
        productName = false,
        category = "' . single_cat_title( '', false ) . '"
        ]);';
				update_option( 'humcommerce_category_view', $c_code );
			}
		}

	}

	/**
	 * Add the JavaScript to the head for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function add_humcommerce_script_to_wp_head() {
		$options = get_option( 'humcommerce_options' );

		if ( array_key_exists( 'host', $options ) ) {
			$host_url = $options['host'];
		} else {
			$host_url = 'app.humdash.com';
		}

		echo ' <!-- HumDash -->
                <script type="text/javascript">
                  var _ha = _ha || []; ' . wp_kses_post( get_option( 'humcommerce_add_to_cart' ) ) . '
                  ' . wp_kses_post( get_option( 'humcommerce_track_order' ) ) . '
                  ' . wp_kses_post( get_option( 'humcommerce_product_view' ) ) . '
                  ' . wp_kses_post( get_option( 'humcommerce_category_view' ) ) . "
                  _ha.push(['trackPageView']);
                  _ha.push(['enableLinkTracking']);
                  (function() {
                    var u=\"//" . esc_js( $host_url ) . "/\";
                    _ha.push(['setTrackerUrl', u+'humdash.php']);
                    _ha.push(['setSiteId', '" . esc_js( $options['si'] ) . "']);
                    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'humdash.js'; s.parentNode.insertBefore(g,s);
                  })();
                </script>
           <!-- End HumDash Code -->";
		delete_option( 'humcommerce_add_to_cart' );
		delete_option( 'humcommerce_track_order' );
		delete_option( 'humcommerce_product_view' );
		delete_option( 'humcommerce_category_view' );
	}

	/**
	 * To update tracking code for updated cart.
	 *
	 * @since    1.0.0
	 */
	public function track_update_cart() {
		global $woocommerce;
		$items = $woocommerce->cart->get_cart();
		$code  = '';
		foreach ( $items as $item => $values ) {
			$_product       = wc_get_product( $values['data']->get_id() );
			$category_terms = get_the_terms( $values['data']->get_id(), 'product_cat' );
			$category_array = $this->get_categories_array( $category_terms );
			$code          .= '
        _ha.push(["addEcommerceItem",
                        "' . $_product->get_sku() . '", 
                        "' . $_product->get_title() . '", 
                        [' . $category_array . '], 
                        ' . get_post_meta( $values['product_id'], '_price', true ) . ', 
                        ' . $values['quantity'] . ' 
                        ]);';
			update_option( 'humcommerce_add_to_cart', $code );
		}

		$total = 0;
		$cart  = $woocommerce->cart;
		if ( ! empty( $cart->total ) ) {
			$total = $cart->total;
		} elseif ( ! empty( $cart->cart_contents_total ) ) {
			$total = $cart->cart_contents_total;
		}
		$code .= '
    _ha.push(["trackEcommerceCartUpdate",' . $total . ']);';
		update_option( 'humcommerce_add_to_cart', $code );
	}

	/**
	 * To update tracking code for updated cart.
	 *
	 * @since    1.0.0
	 * @param string $cart_updated Flag for cart updated.
	 */
	public function track_update_cart_updated( $cart_updated ) {
		$this->track_update_cart();
		return $cart_updated;
	}

	/**
	 * To track ecommerce order.
	 *
	 * @since    1.0.0
	 * @param int $order_id Order id.
	 */
	public function track_ecommerce_order( $order_id ) {
		$code                  = '';
		$order                 = wc_get_order( $order_id );
		$order_number_to_track = $order_id;
		if ( method_exists( $order, 'get_order_number' ) ) {
			$order_number_to_track = $order->get_order_number();
		}

		foreach ( $order->get_items() as $item_key => $item_values ) {
			$product = $item_values->get_product();

			$item_data = $item_values->get_data();

			$product_name   = $item_data['name'];
			$product_id     = $item_data['product_id'];
			$category_terms = get_the_terms( $product_id, 'product_cat' );
			$category_array = $this->get_categories_array( $category_terms );
			$quantity       = $item_data['quantity'];
			$product_sku    = $product->get_sku();
			$product_price  = $product->get_price();
			$code          .= '_ha.push(["addEcommerceItem",
                        "' . $product_sku . '", 
                        "' . $product_name . '", 
                        [' . $category_array . '], 
                        ' . $product_price . ', 
                        ' . $quantity . ' 
                        ]);';
		}

		$shipping_amt = $this->woocommerce_version3() ? $order->get_shipping_total() : $order->get_total_shipping();
		$code        .= '_ha.push(["trackEcommerceOrder",
                "' . $order_number_to_track . '", 
                ' . $order->get_total() . ', 
                ' . $order->get_subtotal() . ', 
                ' . $order->get_cart_tax() . ', 
                ' . $shipping_amt . ', 
                ' . $order->get_total_discount() . ' 
                ]);';
		update_option( 'humcommerce_track_order', $code );
	}

	/**
	 * Compare the currently installed version of WooCommerce.
	 *
	 * @since    1.0.0
	 */
	public function woocommerce_version3() {
		global $woocommerce;
		$result = version_compare( $woocommerce->version, '3.0', '>=' );
		return $result;
	}

	/**
	 * To get Categories array.
	 *
	 * @since    1.0.0
	 * @param array $category_terms Category Terms.
	 */
	public function get_categories_array( $category_terms ) {
		$categories = array();
		if ( is_wp_error( $category_terms ) ) {
			return $categories;
		}
		if ( ! empty( $category_terms ) ) {
			foreach ( $category_terms as $category ) {
				$categories[] = $category->name;
			}
		}
		$categories     = array_unique( $categories );
		$categories     = array_slice( $categories, 0, 5 );
		$category_array = '';
		foreach ( $categories as $category ) {
			$category_array .= '"' . $category . '",';
		}
		return $category_array;
	}
}

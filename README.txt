=== HumCommerce ===
Contributors: humcommerce
Tags: humcommerce, analytics, heatmaps, session recordings
Requires at least: 3.0.1
Tested up to: 5.1.1
Requires PHP: 5.2.4
Stable tag: 2.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The ideal plugin for website visitor recordings, heatmaps, form analytics, A/B testing and more.

== Description ==

[HumCommerce](https://www.humcommerce.com) is a conversion rate optimization (CRO) tool. It provides behavioral analytics (visitor session recordings, Heatmaps, etc.) in addition to traditional analytics (traffic data, traffic source, etc.).

This official HumCommerce extension enables seamless integration between your WooCommerce website and HumCommerce tool. Once integrated, HumCommerce tool will start tracking data of your WooCommerce store. Using this extension, you can also track e-commerce data such as revenue, conversion rate, number of sales, individual product performance, etc.

= Key features of HumCommerce: =

- Visitor Session Recordings - Record visitor sessions and observe what visitors are doing on your website. Find pain points on your website optimized for conversion.

- Heatmaps - See how customers interact with your website. Heatmaps show visitor interaction (click, move, scroll data) for different devices.

- Sales Funnels - Find problems in your sales funnels and optimize them for better conversions. Find at which stage do visitors abandon your website. Improve your funnel conversion rate to maximize profit.

- A/B Testing - Set up A/B testing to compare two versions of a landing page. Compare landing pages to find which color schemes, buttons, or website copy works best to increase conversion rate.

- Form Analytics - Form Analytics gives you powerful insights into how your visitors interact with the forms on your website. Boost form completion rate by adding/removing/editing form fields.

= E-commerce insights like: =

- Product performance - Know how your products are performing based on the revenue, product conversion rate, visits, etc. Product performance gives you an idea of which products should you be focussing on to improve sales.

- Abandoned Carts - Get all the abandoned cart data. You can check how much percentage of your visitors end up abandoning the cart and how much revenue has been left in the cart.

- Sales data - Get insights on your buyers. Know the source of every sale that happens on your website. Sales data can be analyzed to maximize conversions.

This plugin provides a simple installation of HumCommerce on your WordPress website, by providing you instant access to all HumCommerce features.


== Installation ==

1. Download plugin.
2. Upload plugin from Admin -> Plugins -> Add New -> Upload Plugin.

= Manual Alternatives =
1. Extract & upload `humcommerce.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. screenshot-1.png

== Frequently Asked Questions ==

= Do I need a HumCommerce account to use this plugin? =

Yes. You can sign up for a [free HumCommerce plan](https://www.humcommerce.com/lifetime-free/) that provides access to all our features including heatmaps, session recordings/replays, and deep e-commerce integration tools and more.
You can also check out HumComemrce premium plans like Beginner plan, Pro plan and Business plan.
Confused about which plan to choose? Visit our [plan comparison page](https://www.humcommerce.com/pricing/) to know more.

= Should I purchase a paid plan? =

HumCommerce's paid services include more session recordings, more pageviews, priority support, and more.
To learn more about the services we provide, visit our [plan comparison page](https://www.humcommerce.com/pricing?from=wporg).

= What are the HumCommerce Terms of Use? =

The full [terms of use are available here](https://www.humcommerce.com/terms-of-use/) along with other legal and privacy documentation. If you have further questions do not hesitate to reach out to our [support](support@humcommerce.com).

= What is Site ID? =

Login to your HumCommerce Account. Go to My Account. Click on Launch Dashboard button.
The site id number is listed under Settings/Website/Manage in your HumDash administration panel.

= How do I view my stats? =

Once you've installed HumCommerce, you can launch our tool from the "Launch Dashboard" button on My Account page.
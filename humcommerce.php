<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.humcommerce.com
 * @since             1.0.0
 * @package           humcommerce
 *
 * @wordpress-plugin
 * Plugin Name:       HumCommerce
 * Plugin URI:        https://wordpress.org/plugins/humcommerce/
 * Description:       HumCommerce WordPress plugin to Record, Analyze & Convert your visitors.
 * Version:           2.1.3
 * Author:            HumCommerce
 * Author URI:        https://www.humcommerce.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       humcommerce
 * Domain Path:       /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'HUMCOMMERCE_VERSION', '2.1.3' );


require plugin_dir_path( __FILE__ ) . 'includes/class-humcommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_humcommerce() {
	$plugin = new Humcommerce();
	$plugin->run();

}

run_humcommerce();

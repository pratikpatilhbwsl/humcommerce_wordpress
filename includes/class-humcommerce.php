<?php
/**
 * The file that defines the core plugin class.
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since 1.0.0
 *
 * @package humcommerce
 * @subpackage humcommerce/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    humcommerce
 * @subpackage humcommerce/includes
 */
class Humcommerce {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.1
	 * @access   protected
	 * @var      Humcommerce_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.1
	 * @access   protected
	 * @var      string    $humcommerce    The string used to uniquely identify this plugin.
	 */
	protected $humcommerce;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.1
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.1
	 */
	public function __construct() {
		if ( defined( 'HUMCOMMERCE_VERSION' ) ) {
			$this->version = HUMCOMMERCE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->humcommerce = 'humcommerce';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		add_filter( 'plugin_action_links_' . plugin_basename( plugin_dir_path( __DIR__ ) . $this->humcommerce . '.php' ), array( $this, 'humcommerce_plugin_action_links' ), 10 );
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Humcommerce_Loader. Orchestrates the hooks of the plugin.
	 * - Humcommerce_Settings. Defines settings.
	 * - Humcommerce_Admin. Defines all hooks for the admin area.
	 * - Humcommerce_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.1
	 * @access   private
	 */
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-humcommerce-loader.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-humcommerce-admin.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-humcommerce-public.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-humcommerce-settings.php';

		$this->loader = new Humcommerce_Loader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.1
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Humcommerce_Admin( $this->get_humcommerce(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.1
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public = new Humcommerce_Public( $this->get_humcommerce(), $this->get_version() );

		$this->loader->add_action( 'wp', $plugin_public, 'init_hum_commerce' );
		$this->loader->add_action( 'wp_head', $plugin_public, 'add_humcommerce_script_to_wp_head' );
		$this->loader->add_action( 'woocommerce_add_to_cart', $plugin_public, 'track_update_cart', 99999, 0 );
		$this->loader->add_action( 'woocommerce_cart_item_removed', $plugin_public, 'track_update_cart', 99999, 0 );
		$this->loader->add_action( 'woocommerce_cart_item_restored', $plugin_public, 'track_update_cart', 99999, 0 );
		$this->loader->add_filter( 'woocommerce_update_cart_action_cart_updated', $plugin_public, 'track_update_cart_updated', 99999, 1 );
		$this->loader->add_action( 'woocommerce_applied_coupon', $plugin_public, 'track_update_cart', 99999, 0 );
		$this->loader->add_action( 'woocommerce_removed_coupon', $plugin_public, 'track_update_cart', 99999, 0 );
		$this->loader->add_action( 'woocommerce_order_status_completed', $plugin_public, 'track_ecommerce_order' );
	}

	/**
	 * Returns the plugin name.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_humcommerce() {
		return $this->humcommerce;
	}

	/**
	 * Returns the plugin version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Runs the plugin.
	 *
	 * @since 1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * Returns the Loader instance.
	 *
	 * $since 1.0.0
	 *
	 * @return Humcommerce_Loader
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Returns plugin action links.
	 *
	 * @since 1.0.0
	 * @param string $links Links to be updated.
	 *
	 * @return array
	 */
	public function humcommerce_plugin_action_links( $links ) {
		$setting_url = 'admin.php?page=humcommerce-settings';
		$links[]     = '<a href="' . get_admin_url( null, $setting_url ) . '">Settings</a>';
		return $links;
	}
}
